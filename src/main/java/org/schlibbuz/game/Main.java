/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schlibbuz.game;

import de.gurkenlabs.litiengine.Game;
import de.gurkenlabs.litiengine.GameListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author stefan
 */
public class Main {
  private static final Logger LOGGER = LogManager.getLogger(Main.class);
  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    Game.addGameListener(new GameListener() {
      @Override
      public void initialized(String... args) {
        System.out.println("game init");
      }

      @Override
      public void started() {
        System.out.println("game started");
      }

      @Override
      public void terminated() {
        System.out.println("game terminated");
      }
    });
    Game.init(args);
    Game.start();
  }

}
